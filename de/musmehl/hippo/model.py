#  Copyright (c) 2021. by Sven Prüfer
#
#  This file is part of Hippo.
#
#  Hippo is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Hippo is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Hippo.  If not, see <https://www.gnu.org/licenses/>.
from dataclasses import dataclass
from enum import Enum
from datetime import date


class Place(str, Enum):
    """
    Places that are configurable and defined in Hippo.
    """
    KOCHEL_AM_SEE = 'Kochel am See'
    GARMISCH_PARTENKIRCHEN = 'Garmisch-Partenkirchen'
    MUENCHEN = 'München'


@dataclass(frozen=True)
class Forecast:
    """
    A weather forecast for a specific date and place.
    """
    place: Place
    date: date
    rain_probability: float
    rain_amount: float
    temperature_low: float
    temperature_high: float
    sun_duration: float
    lightning_probability: float
    snow_new: float
    snow_altitude: float


class Season(str, Enum):
    """
    Summer or Winter.
    """
    SUMMER = 'Summer'
    WINTER = 'Winter'
