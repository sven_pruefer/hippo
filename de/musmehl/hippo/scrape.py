#  Copyright (c) 2021. by Sven Prüfer
#
#  This file is part of Hippo.
#
#  Hippo is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Hippo is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Hippo.  If not, see <https://www.gnu.org/licenses/>.

import re
from abc import ABC, abstractmethod
from datetime import date
from typing import Dict, List
import requests
from bs4 import BeautifulSoup, ResultSet
from requests import Response

from de.musmehl.hippo.util import combine_probabilities
from model import Place, Forecast, Season


class WeatherWebsiteScraper(ABC):
    """
    Base class for weather website scrapers
    """

    @abstractmethod
    def get_forecast(self, requested_date: date, today: date) -> Forecast:
        """
        Gets forecast for a specific date and place.

        :param today: Today's date
        :param requested_date: Date for which to get the forecast
        :return: Found forecast
        """


class BergfexScraper(WeatherWebsiteScraper):
    """
    Scrape bergfex.de for weather data.
    """

    _base_url: str = """https://www.bergfex.de"""
    _season_dict: Dict[Season, str] = {Season.SUMMER: "sommer", Season.WINTER: "winter"}
    _place_dict: Dict[Place, str] = {Place.KOCHEL_AM_SEE: "kochel-am-see",
                                     Place.GARMISCH_PARTENKIRCHEN: "garmisch-partenkirchen",
                                     Place.MUENCHEN: "muenchen"}

    def __init__(self, season: Season, place: Place):
        """
        Initialize a new BergfexScraper instance.
        """
        self._season: Season = season
        self._place: Place = place

    def _get_url(self) -> str:
        return f"""{self._base_url}/{self._season_dict[self._season]}/{self._place_dict[self._place]}""" + \
               """/wetter/prognose/#day1"""

    def get_forecast(self, requested_date: date, today: date) -> Forecast:
        # Get website content
        response: Response = requests.get(self._get_url())
        if response.status_code != 200:
            raise RuntimeError(f"Bergfex responded with http code {response.status_code} and reason {response.reason}")
        website = BeautifulSoup(response.text, 'html.parser')
        return self._interpret_result(website, requested_date, today)

    def _interpret_result(self, website: BeautifulSoup, requested_date: date, today: date) -> Forecast:
        # Determine relevant content for requested_date
        diff_date: int = (requested_date - today).days
        if diff_date < 0 or 4 < diff_date:
            raise ValueError(f"Bergfex only supports detailed forecasts for 0 - 4 days, but {diff_date} was requested")

        forecasts: ResultSet = website \
            .find(id=f"forecast-day-{diff_date}-intervals", class_="intervals3h", recursive=True) \
            .find(class_="forecast9d-intervals", recursive=True) \
            .find_all(class_="interval fields")

        rain_probabilities: List[float] = []
        rain_amounts: List[float] = []
        temperatures: List[float] = []
        sun_durations: List[float] = []
        lightning_probabilities: List[float] = []
        snow_new_amount: List[float] = []
        snow_altitudes: List[float] = []

        for forecast in forecasts:
            # The Bergfex data is strange, temperatures seems to belong to a particular 'time offset',
            # but rain probabilities are in between. You can see this in the rendered website.
            # We thus distinguish between two types of intervals and respective contents.

            if forecast.find(class_="time offset").string in ['08:00', '11:00', '14:00', '17:00']:
                # Parse temperatures. Bergfex includes both valley and mountain top temperatures,
                # so if both are available, we consider the ones for the valley, i.e. the last values
                # Values look like '12°C'
                temperatures.append(float(forecast.find_all(class_='tmax')[-1].string.strip()[:-2]))

            if forecast.find(class_="time offset").string in ['11:00', '14:00', '17:00']:
                # Parse rain amounts. Value may be either of the form '-' or '3,5l'
                if forecast.find(class_="rrr").string.strip() == '-':
                    rain_amounts.append(0)
                else:
                    rain_amounts.append(float(forecast.find(class_="rrr").string.strip().replace(',', '.')[:-1]))

                # Parse rain probabilities. Values look like '40% Regenwahrscheinlichkeit'
                rain_probabilities.append(float(forecast.find(class_="rrp")['title'].strip().split('%')[0]) / 100)

                # Parse lightning probabilities. Values look like 'Gewitterwahrscheinlichkeit: 20%'
                lightning_probabilities.append(
                    float(forecast.find(class_="wgew")['title'].strip().split(' ')[1][:-1]) / 100)

                # Parse snow altitude. Values look like '3.950m'
                snow_altitudes.append(float(forecast.find(class_="sgrenze").string.strip().replace('.', '')[:-1]))

                # Parse sun durations. Values may look like 'wolkenlos' or 'heiter 1.20h Sonne'
                sun_string = forecast.find(name='img', src=re.compile('^.*/images/wetter/.*'))['title']
                if "h Sonne" in sun_string:
                    sun_durations.append(float(sun_string.strip().split(' ')[1][:-1]))
                else:
                    sun_durations.append(0)

        return Forecast(
            place=self._place,
            date=requested_date,
            rain_probability=combine_probabilities(rain_probabilities),
            rain_amount=sum(rain_amounts),
            temperature_low=min(temperatures),
            temperature_high=max(temperatures),
            sun_duration=sum(sun_durations) / (3 * len(sun_durations)),
            lightning_probability=combine_probabilities(lightning_probabilities),
            snow_new=0.0,  # TODO How to get new snow amounts? Maybe this exists only during winter?
            snow_altitude=min(snow_altitudes)
        )
