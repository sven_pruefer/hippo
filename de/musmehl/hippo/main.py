#  Copyright (c) 2021. by Sven Prüfer
#
#  This file is part of Hippo.
#
#  Hippo is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Hippo is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Hippo.  If not, see <https://www.gnu.org/licenses/>.
from datetime import date

from de.musmehl.hippo.model import Season, Place, Forecast
from de.musmehl.hippo.scrape import BergfexScraper


def main() -> None:
    """
    Main entry point of Hippo.

    :return: None
    """
    bergfex_garmisch_scraper = BergfexScraper(Season.SUMMER, Place.GARMISCH_PARTENKIRCHEN)
    result: Forecast = bergfex_garmisch_scraper.get_forecast(requested_date=date(2021, 6, 18), today=date(2021, 6, 14))

    print(result)


if __name__ == "__main__":
    main()
