#  Copyright (c) 2021. by Sven Prüfer
#
#  This file is part of Hippo.
#
#  Hippo is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Hippo is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Hippo.  If not, see <https://www.gnu.org/licenses/>.
from de.musmehl.hippo.util import combine_probabilities
from pytest import approx


def test_combine_probabilities():
    probabilities = [n / 365 for n in range(23)]
    print(probabilities)
    assert combine_probabilities(probabilities) == approx(0.5073, abs=0.001)
